import * as fs from 'fs';
import fetch from 'node-fetch';
import * as dotenv from 'dotenv';
import {getFigmaFile, getFigmaSvg} from "./figmaImport";
import svgtofont from 'svgtofont';
import path from "path";


dotenv.config();

interface FigmaNode {
    name: string;
    children?: FigmaNode[];
    type: string;
    id: string;
}

function getComponents(root: FigmaNode): FigmaNode[] {
    const components = [];
    if (root.type === "COMPONENT")
        return [root];
    if (root.children === undefined)
        return [];
    for (const child of root.children) {
        components.push(...getComponents(child))
    }
    return components;
}

async function saveSvg(icon: FigmaNode, url: string, genFolder: string) {
    const res = await fetch(url);
    let svg = await res.text();
    svg = svg
        .replace(/fill="#333333"/g, "fill=\"currentColor\"")
        .replace(/fill="black"/g, "fill=\"currentColor\"");
    const filename = icon.name
        .replace(/\//g, "-")
        .replace(/-24px/g, "")
        .replace(/_24px/g, "")
        .replace(/ /g, "-")
        .toLowerCase();
    fs.writeFileSync(`${genFolder}/${filename}.svg`, svg)
}

async function main(option: { outFolder: string }) {
    const genFolder = option.outFolder;
    if (!fs.existsSync(genFolder)) {
        fs.mkdirSync(genFolder);
    }

    const iconFile: any = await getFigmaFile(process.env.FIGMA_ICONFILE as string);


    const icons = getComponents(iconFile.document as FigmaNode);
    const figmaIds = icons.map(v => v.id);
    const urlSvgs = await getFigmaSvg(process.env.FIGMA_ICONFILE as string, figmaIds.join(','))
    const promises = [];
    for (const icon of icons) {
        promises.push(saveSvg(icon, urlSvgs[icon.id], genFolder))
    }
    await Promise.all(promises)
    await svgtofont({
        src: path.resolve(process.cwd(), option.outFolder), // svg path
        dist: path.resolve(process.cwd(), 'fonts'), // output path
        fontName: 'openbridge-icon', // font name
        css: true, // Create CSS files.
        svgicons2svgfont: {
            fontHeight: 24,
            normalize: true
        },
    });
    /*
    const figmaIds = elements.map(v => v.element.id);
    const urlSvgs = await getFigmaSvg(mainFigmaFile, figmaIds.join(','))
    const promises = [];
    for (const ele of elements) {
      // eslint-disable-next-line no-async-promise-executor
      promises.push(new Promise( async (resolve, reject) => {
        const element = ele.element;
      const component = ele.component;
      console.log(`Exporting ${ component.name }`);

      if (element === null) {
        console.error(`Could not find ${ component.name }`);
        reject();
        return;
      }
      let imageData
      try {
        imageData = await fetch(urlSvgs[element.id])
      } catch (e) {
        console.error(`Could not download SVG for ${ component.name }`,e);
        reject(e);
        return;
      }
      const svg = await imageData.text()
      const out = convertSvg(
        (element as unknown) as FrameNode,
        svg,
        styles,
        option.removeAttributes,
        element.name
      );

      const outputFolder = component.outputFolder
        ? `${ genFolder }/${ component.outputFolder }`
        : genFolder;

      if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder);
      }
      fs.writeFileSync(`${ outputFolder }/${ component.name }.svg`, out);
      }))


    }
    Promise.all(promises).then(()=>console.log("Done"));
     */
}

main({outFolder: "svg"})
//.then(() => console.log('Completed autogenerate'))
//.catch(e => console.error('Failed autogenerate \nError:\n', e))
